#include <omp.h>
#include <bits/stdc++.h>
using namespace std;

int main() {
    
  int a[1000];
  int b[1000];
  int c[1000];
  int count = 0;
  double time = omp_get_wtime();
  
  
  for(int i=0;i<1000;i++)
      {
        a[i]=i;
        b[i] = i;
      }
  
  
  #pragma omp parallel for schedule(static,200)
  for(int i=0;i<1000;i++) {
    c[i] = a[i] + b[i];
    
  }
  
  for(int i=0;i<1000;i++)
    printf("%d ",c[i]);
    
  printf("\n \n %d Times loop executed",count);
  printf("\n Using %d no of threads and %f execution time",omp_get_max_threads(),omp_get_wtime() - time);
  
}
